package ru.karamyshev.taskmanager.exception;

public class MatchLoginsException extends RuntimeException {

    public MatchLoginsException() {
        super("Error! This login already exists...");
    }
}
