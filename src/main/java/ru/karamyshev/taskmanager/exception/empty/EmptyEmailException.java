package ru.karamyshev.taskmanager.exception.empty;

public class EmptyEmailException extends RuntimeException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }
}
