package ru.karamyshev.taskmanager.constant;

public interface MsgCommandConst {

    String DESCRIPTION_HELP = "Display terminal commands.";

    String DESCRIPTION_VERSION = "Show version info.";

    String DESCRIPTION_ABOUT = "Show developer info.";

    String DESCRIPTION_EXIT = "Close application.";

    String DESCRIPTION_INFO = "Display information about system.";

    String DESCRIPTION_COMMAND = "Show program commands.";

    String DESCRIPTION_ARGUMENT = "Show program arguments.";

    String COMMAND_EMPTY = "\n Line is empty.";

    String COMMAND_N_FOUND = " \n Error! Command not found.";

    String ARGS_N_FOUND = " \n Error! Arguments not found.";

    String COMMAND_ABSENT = "\n Error! Command absent.";

    String TASK_CREATE = "Create new task.";

    String TASK_REMOVE = "Remove all tasks.";

    String TASK_LIST = "Show task list.";

    String PROJECT_CREATE = "Create new project.";

    String PROJECT_REMOVE = "Remove all projects.";

    String PROJECT_LIST = "Show project list.";

    String TASK_UPDATE_BY_INDEX = "Update task by index.";

    String TASK_UPDATE_BY_ID = "Update task by id.";

    String TASK_VIEW_BY_ID = "Show task by id.";

    String TASK_VIEW_BY_INDEX = "Show task by index.";

    String TASK_VIEW_BY_NAME = "Show task by name.";

    String TASK_REMOVE_BY_ID = "Remove task by id.";

    String TASK_REMOVE_BY_INDEX = "Remove task by index.";

    String TASK_REMOVE_BY_NAME = "Remove task by name.";

    String PROJECT_UPDATE_BY_INDEX = "Update project by index.";

    String PROJECT_UPDATE_BY_ID = "Update project by id.";

    String PROJECT_VIEW_BY_ID = "Show project by id.";

    String PROJECT_VIEW_BY_INDEX = "Show project by index.";

    String PROJECT_VIEW_BY_NAME = "Show project by name.";

    String PROJECT_REMOVE_BY_ID = "Remove project by id.";

    String PROJECT_REMOVE_BY_INDEX = "Remove project by index.";

    String PROJECT_REMOVE_BY_NAME = "Remove project by name.";

    String LOGIN = "Login in account.";

    String LOGOUT = "Logout in account.";

    String REGISTRY = "Registration new account.";

    String SHOW_PROFILE = "Show profile.";

    String RENAME_PASSWORD = "Rename password account.";

    String RENAME_LOGIN = "Rename login account";
}
