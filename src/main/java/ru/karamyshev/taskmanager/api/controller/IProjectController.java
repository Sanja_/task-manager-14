package ru.karamyshev.taskmanager.api.controller;

public interface IProjectController {

    void showProject();

    void clearProject();

    void createProject();

    void createProjects();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();
}
