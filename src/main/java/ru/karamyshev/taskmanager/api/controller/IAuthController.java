package ru.karamyshev.taskmanager.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registry();

}
