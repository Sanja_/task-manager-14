package ru.karamyshev.taskmanager.api.controller;

public interface IUserController {

    void renamePassword();

    void renameLogin();

    void showProfile();
}
