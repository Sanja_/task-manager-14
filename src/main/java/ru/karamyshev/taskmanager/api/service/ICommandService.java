package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
