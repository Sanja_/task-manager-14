package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.entity.User;

public interface IAuthService {

    String getUserId();

    String getCurrentLogin();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

    void renameLogin(String userId, String currentLogin, String newLogin);

    User showProfile(String userId, String login);

    void renamePassword(
            String userId,
            String currentLogin,
            String oldPassword,
            String newPassword
    );
}
