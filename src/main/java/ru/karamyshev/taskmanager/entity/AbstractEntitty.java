package ru.karamyshev.taskmanager.entity;

import java.util.UUID;

public class AbstractEntitty {

    private long id = UUID.randomUUID().getMostSignificantBits();

    public long  getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

}
