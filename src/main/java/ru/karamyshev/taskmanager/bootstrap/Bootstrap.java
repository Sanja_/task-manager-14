package ru.karamyshev.taskmanager.bootstrap;

import ru.karamyshev.taskmanager.api.controller.*;
import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.*;
import ru.karamyshev.taskmanager.constant.ArgumentConst;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.constant.TerminalConst;
import ru.karamyshev.taskmanager.controller.*;
import ru.karamyshev.taskmanager.exception.CommandIncorrectException;
import ru.karamyshev.taskmanager.repository.CommandRepository;
import ru.karamyshev.taskmanager.repository.ProjectRepository;
import ru.karamyshev.taskmanager.repository.TaskRepository;
import ru.karamyshev.taskmanager.repository.UserRepository;
import ru.karamyshev.taskmanager.role.Role;
import ru.karamyshev.taskmanager.service.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private IAuthController authController = new AuthController(authService);

    private final IUserController userController = new UserController(authService);

    private void initUser(){
        userService.create("test", "test", "test@test.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        initUser();
        inputCommand();
    }

    private void inputCommand() {
        while (true) {
            try {
                parsCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) chooseResponsCommand(arg);
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        try {
            for (String arg : args) chooseResponsArg(arg.trim());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }

    }

    private void validateArgs(final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    private void chooseResponsArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            case ArgumentConst.ARGUMENTS: commandController.showArguments(); break;
            case ArgumentConst.COMMANDS: commandController.showCommands(); break;
            case ArgumentConst.TASK_LIST: taskController.showTasks(); break;
            case ArgumentConst.TASK_CREATE: taskController.createTasks(); break;
            case ArgumentConst.TASK_CLEAR: taskController.clearTasks(); break;
            case ArgumentConst.PROJECT_LIST: projectController.showProject(); break;
            case ArgumentConst.PROJECT_CREATE: projectController.createProject(); break;
            case ArgumentConst.PROJECT_CLEAR: projectController.clearProject(); break;
            case ArgumentConst.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case ArgumentConst.TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case ArgumentConst.TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case ArgumentConst.TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case ArgumentConst.TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case ArgumentConst.TASK_REMOVE_BY_ID : taskController.removeTaskById(); break;
            case ArgumentConst.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case ArgumentConst.TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case ArgumentConst.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case ArgumentConst.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case ArgumentConst.PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case ArgumentConst.PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case ArgumentConst.PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case ArgumentConst.PROJECT_REMOVE_BY_ID : projectController.removeProjectById(); break;
            case ArgumentConst.PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case ArgumentConst.PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); break;
            case ArgumentConst.LOGIN: authController.login(); break;
            case ArgumentConst.LOGOUT: authController.logout(); break;
            case ArgumentConst.REGISTRY: authController.registry(); break;
            case ArgumentConst.SHOW_PROFILE: userController.showProfile(); break;
            case ArgumentConst.RENAME_PASSWORD: userController.renamePassword(); break;
            case ArgumentConst.RENAME_LOGIN: userController.renameLogin(); break;
            default: throw new CommandIncorrectException(arg);
        }
    }

    private void chooseResponsCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT: commandController.showAbout(); break;
            case TerminalConst.VERSION: commandController.showVersion(); break;
            case TerminalConst.HELP: commandController.showHelp(); break;
            case TerminalConst.INFO: commandController.showInfo(); break;
            case TerminalConst.EXIT: commandController.exit(); break;
            case TerminalConst.ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.COMMANDS: commandController.showCommands(); break;
            case TerminalConst.TASK_LIST: taskController.showTasks(); break;
            case TerminalConst.TASK_CREATE: taskController.createTasks(); break;
            case TerminalConst.TASK_CLEAR: taskController.clearTasks(); break;
            case TerminalConst.PROJECT_LIST: projectController.showProject(); break;
            case TerminalConst.PROJECT_CREATE: projectController.createProject(); break;
            case TerminalConst.PROJECT_CLEAR: projectController.clearProject(); break;
            case TerminalConst.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case TerminalConst.TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case TerminalConst.TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case TerminalConst.TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case TerminalConst.TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case TerminalConst.TASK_REMOVE_BY_ID : taskController.removeTaskById(); break;
            case TerminalConst.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConst.TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case TerminalConst.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case TerminalConst.PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case TerminalConst.PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case TerminalConst.PROJECT_REMOVE_BY_ID : projectController.removeProjectById(); break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); break;
            case TerminalConst.LOGIN: authController.login(); break;
            case TerminalConst.REGISTRY: authController.registry(); break;
            case TerminalConst.LOGOUT: authController.logout(); break;
            case TerminalConst.SHOW_PROFILE: userController.showProfile(); break;
            case TerminalConst.RENAME_PASSWORD: userController.renamePassword(); break;
            case TerminalConst.RENAME_LOGIN: userController.renameLogin(); break;
            default: throw new CommandIncorrectException(command);
        }
    }
}
