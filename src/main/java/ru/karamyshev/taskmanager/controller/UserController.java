package ru.karamyshev.taskmanager.controller;

import ru.karamyshev.taskmanager.api.controller.IUserController;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserController implements IUserController {

    private IAuthService authService;

    public UserController(IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void renamePassword() {
        System.out.println("CHANGE ACCOUNT PASSWORD");
        String userId = authService.getUserId();
        System.out.println("[ENTER OLD PASSWORD]");
        String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD]");
        String newPassword = TerminalUtil.nextLine();
        String currentLogin = authService.getCurrentLogin();
        authService.renamePassword(userId, currentLogin , oldPassword, newPassword);
        System.out.println("[OK]");
    }

    @Override
    public void renameLogin() {
        System.out.println("CHANGE ACCOUNT LOGIN");
        String currentLogin = authService.getCurrentLogin();
        System.out.println("CURRENT LOGIN: " + currentLogin);
        String userId = authService.getUserId();
        System.out.println("[ENTER NEW LOGIN]");
        String newLogin = TerminalUtil.nextLine();
        authService.renameLogin(userId, currentLogin, newLogin);
        System.out.println("[OK]");
    }

    @Override
    public void showProfile() {
        String currentLogin = authService.getCurrentLogin();
        System.out.println("[SHOW PROFILE]");
        String userId = authService.getUserId();
        User user = authService.showProfile(userId, currentLogin);
        System.out.println("LOGIN: " +user.getLogin());
        System.out.println("HASH PASSWORD: " +user.getPasswordHash());
        System.out.println("[OK]");
    }
}
