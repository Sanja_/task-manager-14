package ru.karamyshev.taskmanager.controller;

import ru.karamyshev.taskmanager.api.controller.IAuthController;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.util.HashUtil;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class AuthController implements IAuthController {

    private IAuthService authService;

    public AuthController(IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

}
